package elkware.cityguide.com.onemyguide;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;


import elkware.cityguide.com.onemyguide.Fragments.Places_to_Visite;
import elkware.cityguide.com.onemyguide.Fragments.Attraction_Fragment;
import elkware.cityguide.com.onemyguide.Fragments.Bank_Fragment;
import elkware.cityguide.com.onemyguide.Fragments.Foods_fragment;
import elkware.cityguide.com.onemyguide.Fragments.Hotels_fragment;
import elkware.cityguide.com.onemyguide.Fragments.NightLife_Fragment;
import elkware.cityguide.com.onemyguide.Fragments.Public_Service_Fragment;
import elkware.cityguide.com.onemyguide.Fragments.Shopping_Fragment;
import elkware.cityguide.com.onemyguide.Fragments.Sports_Fragment;
import elkware.cityguide.com.onemyguide.Fragments.Transport_Fragment;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new Places_to_Visite()).commit();
        }
        setContentView(R.layout.activity_main);
        mDrawerLayout = findViewById(R.id.drawer);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.Open, R.string.Close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        setSupportActionBar(toolbar);

        NavigationView navigationView = findViewById(R.id.navdraw);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupDrawerContent(navigationView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    boolean doubleBackToExitPressedOnce = false;


    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new Places_to_Visite()).commit();
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 3000);

    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        android.support.v4.app.Fragment fragment = null;

        switch (menuItem.getItemId()) {
            case R.id.place_to_visite:
                fragment = new Places_to_Visite();
                break;
            case R.id.favourite:
                Intent intent = new Intent(getBaseContext(), Favourites.class);
                startActivity(intent);
                break;
            case R.id.food_drink:
                fragment = new Foods_fragment();
                break;
            case R.id.hotels:
                fragment = new Hotels_fragment();
                break;
            case R.id.attraction:
                fragment = new Attraction_Fragment();
                break;
            case R.id.sports:
                fragment = new Sports_Fragment();
                break;
            case R.id.night_life:
                fragment = new NightLife_Fragment();
                break;
            case R.id.shopping:
                fragment = new Shopping_Fragment();
                break;
            case R.id.transport:
                fragment = new Transport_Fragment();
                break;
            case R.id.public_services:
                fragment = new Public_Service_Fragment();
                break;
            case R.id.bank:
                fragment = new Bank_Fragment();
                break;
            case R.id.about:
                Intent i = new Intent(getBaseContext(), About.class);
                startActivity(i);
                break;


            default:
                fragment = new Places_to_Visite();
        }


        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.flContent, fragment);
            ft.commit();
        }


        // Highlight the selected item has been done by NavigationView
        //menuItem.setChecked(true);
        // Set action bar title
        setTitle(menuItem.getTitle());
        // Close the navigation drawer
        mDrawerLayout.closeDrawers();
    }


}
