package elkware.cityguide.com.onemyguide.DetailsActivities;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import elkware.cityguide.com.onemyguide.R;

public class Places_Activity extends AppCompatActivity {
    android.support.v7.widget.Toolbar toolbar;
    TextView mTextView;
    ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);
         toolbar = findViewById(R.id.place_toolbar);
         setSupportActionBar(toolbar);
         getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mTextView = findViewById(R.id.place_descrip2);
        imageView = findViewById(R.id.place_img);

        String name = getIntent().getExtras().getString("place_name");
        imageView.setImageResource(getIntent().getIntExtra("place_image",00));
        mTextView.setText(name);
        CollapsingToolbarLayout mplace_collapsing = findViewById(R.id.place_collapsing);
        mplace_collapsing.setTitle(name);

        FloatingActionButton mFloatingActionButton = findViewById(R.id.myfloating);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(),"Saved",Toast.LENGTH_LONG).show();
            }
        });




    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;

    }
}
