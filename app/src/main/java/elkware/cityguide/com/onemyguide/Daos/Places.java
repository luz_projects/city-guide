package elkware.cityguide.com.onemyguide.Daos;

public class Places {

    private String place_name;
    private String place_adresse;
    private int place_image;
    private String place_descrip;

    public Places(String place_name, String place_adresse, int place_image, String place_descrip) {
        this.place_name = place_name;
        this.place_adresse = place_adresse;
        this.place_image = place_image;
        this.place_descrip = place_descrip;
    }

    public String getPlace_name() {
        return place_name;
    }

    public void setPlace_name(String place_name) {
        this.place_name = place_name;
    }

    public String getPlace_adresse() {
        return place_adresse;
    }

    public void setPlace_adresse(String place_adresse) {
        this.place_adresse = place_adresse;
    }

    public int getPlace_image() {
        return place_image;
    }

    public void setPlace_image(int place_image) {
        this.place_image = place_image;
    }

    public String getPlace_descrip() {
        return place_descrip;
    }

    public void setPlace_descrip(String place_descrip) {
        this.place_descrip = place_descrip;
    }

    public Places() {
    }
}
