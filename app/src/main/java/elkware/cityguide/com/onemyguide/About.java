package elkware.cityguide.com.onemyguide;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

public class About extends AppCompatActivity {
    private TextView textView,mTextView;
    LinearLayout linearLayout;
Toolbar toolbar;
private Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        textView = findViewById(R.id.credits);
        mTextView = findViewById(R.id.credit2);
        toolbar = findViewById(R.id.toolbar);
        linearLayout = findViewById(R.id.liner);

       mTextView.setText("Wilson Delsol\n"+"Luz DeMars\n"+"Serge4nt Sh4dow\n"+"Diamond Black");
        animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.credits);
        linearLayout.startAnimation(animation);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("About Us");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;

    }
}
