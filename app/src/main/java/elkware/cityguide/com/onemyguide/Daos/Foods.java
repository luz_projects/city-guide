package elkware.cityguide.com.onemyguide.Daos;

public class Foods {
    private String fd_tilte;
    private String fd_descript;
    private String fd_lieu;
    private int fd_Image;

    public Foods(String fd_tilte, String fd_descript, String fd_lieu, int fd_Image) {
        this.fd_tilte = fd_tilte;
        this.fd_descript = fd_descript;
        this.fd_lieu = fd_lieu;
        this.fd_Image = fd_Image;
    }

    public String getFd_tilte() {
        return fd_tilte;
    }

    public void setFd_tilte(String fd_tilte) {
        this.fd_tilte = fd_tilte;
    }

    public String getFd_descript() {
        return fd_descript;
    }

    public void setFd_descript(String fd_descript) {
        this.fd_descript = fd_descript;
    }

    public String getFd_lieu() {
        return fd_lieu;
    }

    public void setFd_lieu(String fd_lieu) {
        this.fd_lieu = fd_lieu;
    }

    public int getFd_Image() {
        return fd_Image;
    }

    public void setFd_Image(int fd_Image) {
        this.fd_Image = fd_Image;
    }

    public Foods() {
    }
}
