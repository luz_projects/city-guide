package elkware.cityguide.com.onemyguide.Adarpters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import elkware.cityguide.com.onemyguide.Daos.Foods;
import elkware.cityguide.com.onemyguide.R;

public class Food_Adapter extends RecyclerView.Adapter<Food_Adapter.MyViewHolder> {
     Context mContext;
     List<Foods> mFoods;

    public Food_Adapter(Context mContext, List<Foods> mFoods) {
        this.mContext = mContext;
        this.mFoods = mFoods;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.items_foods,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(v);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.food_image.setImageResource(mFoods.get(position).getFd_Image());
        holder.food_name.setText(mFoods.get(position).getFd_tilte());

    }

    @Override
    public int getItemCount() {
        return mFoods.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView food_image;
        TextView food_name;
        public MyViewHolder(View itemView) {
            super(itemView);

            food_image = itemView.findViewById(R.id.image_food);
            food_name = itemView.findViewById(R.id.food_name);
        }
    }
}
