package elkware.cityguide.com.onemyguide.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import elkware.cityguide.com.onemyguide.Adarpters.Food_Adapter;
import elkware.cityguide.com.onemyguide.Daos.Foods;
import elkware.cityguide.com.onemyguide.R;


public class Foods_fragment extends Fragment {
    View rootView;
    RecyclerView recyclerView;
    List<Foods> foodsList;

    private CollapsingToolbarLayout collapsingToolbarLayout;


    public Foods_fragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        foodsList = new ArrayList<>();

        foodsList.add(new Foods(
                "Barbecue",
                "Thomas",
                "Eric",
                R.drawable.barbecue
        ));
        foodsList.add(new Foods(
                "Burger",
                "Thomas",
                "Eric",
                R.drawable.burger
        ));
        foodsList.add(new Foods(
                "Salade",
                "Thomas",
                "Eric",
                R.drawable.eating
        ));
        foodsList.add(new Foods(
                "Tapioca",
                "Thomas",
                "Eric",
                R.drawable.food1
        ));
        foodsList.add(new Foods(
                "Croissant",
                "Thomas",
                "Eric",
                R.drawable.food10
        ));
        foodsList.add(new Foods(
                "Riz Blanc",
                "Thomas",
                "Eric",
                R.drawable.food2
        ));
        foodsList.add(new Foods(
                "Concombre",
                "Thomas",
                "Eric",
                R.drawable.food3
        ));
        foodsList.add(new Foods(
                "Thé Chinois",
                "Thomas",
                "Eric",
                R.drawable.food4
        ));
        foodsList.add(new Foods(
                "Coquillage",
                "Thomas",
                "Eric",
                R.drawable.food5
        ));
        foodsList.add(new Foods(
                "Crevette",
                "Thomas",
                "Eric",
                R.drawable.food6
        ));
        foodsList.add(new Foods(
                "Raspberry",
                "Thomas",
                "Eric",
                R.drawable.food7
        ));
        foodsList.add(new Foods(
                "Coconut",
                "Thomas",
                "Eric",
                R.drawable.food8
        ));
        foodsList.add(new Foods(
                "Ice Cream",
                "Thomas",
                "Eric",
                R.drawable.icecreamcone
        ));


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_foods, container, false);
        recyclerView = rootView.findViewById(R.id.food_recycleView);
        Food_Adapter first_adapter = new Food_Adapter(getContext(), foodsList);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerView.setAdapter(first_adapter);
        collapsingToolbarLayout = rootView.findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitleEnabled(true);


        return rootView;
    }


}
