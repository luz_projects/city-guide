package elkware.cityguide.com.onemyguide;

public interface OnLoadMoreListener {
    void onLoadMore();
}
